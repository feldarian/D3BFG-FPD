/*
===========================================================================

Doom 3 BFG Edition GPL Source Code
Copyright (C) 1993-2012 id Software LLC, a ZeniMax Media company.

This file is part of the Doom 3 BFG Edition GPL Source Code ("Doom 3 BFG Edition Source Code").

Doom 3 BFG Edition Source Code is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Doom 3 BFG Edition Source Code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Doom 3 BFG Edition Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, the Doom 3 BFG Edition Source Code is also subject to certain additional terms. You should have received a copy of these additional terms immediately following the terms and conditions of the GNU General Public License which accompanied the Doom 3 BFG Edition Source Code.  If not, please request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc., Suite 120, Rockville, Maryland 20850 USA.

===========================================================================
*/

#ifndef __MODEL_LOCAL_H__
#define __MODEL_LOCAL_H__

/*
===============================================================================

	Static model

===============================================================================
*/

class idJointMat;
struct deformInfo_t;
class ColladaParser; // RB: Collada support

class idRenderModelStatic : public idRenderModel
{
public:
	// the inherited public interface
	static idRenderModel* 		Alloc();
	
	idRenderModelStatic();
	virtual						~idRenderModelStatic();

	void				InitFromFile( const char* fileName ) override;
	bool				LoadBinaryModel( idFile* file, const ID_TIME_T sourceTimeStamp ) override;
	void				WriteBinaryModel( idFile* file, ID_TIME_T* _timeStamp = nullptr ) const override;

	bool				SupportsBinaryModel() override
	{
		return true;
	}
	
	// RB begin
	void				ExportOBJ( idFile* objFile, idFile* mtlFile, ID_TIME_T* _timeStamp = nullptr ) const override;
	// RB end

	void				PartialInitFromFile( const char* fileName ) override;
	void				PurgeModel() override;
	void				Reset() override {};
	void				LoadModel() override;
	bool				IsLoaded() override;
	void				SetLevelLoadReferenced( bool referenced ) override;
	bool				IsLevelLoadReferenced() override;
	void				TouchData() override;
	void				InitEmpty( const char* name ) override;
	void				AddSurface( modelSurface_t surface ) override;
	void				FinishSurfaces() override;
	void				FreeVertexCache() override;
	const char* 		Name() const override;
	void				Print() const override;
	void				List() const override;
	int					Memory() const override;
	ID_TIME_T				Timestamp() const override;
	int					NumSurfaces() const override;
	int					NumBaseSurfaces() const override;
	const modelSurface_t* Surface( int surfaceNum ) const override;
	srfTriangles_t* 	AllocSurfaceTriangles( int numVerts, int numIndexes ) const override;
	void				FreeSurfaceTriangles( srfTriangles_t* tris ) const override;
	bool				IsStaticWorldModel() const override;
	dynamicModel_t		IsDynamicModel() const override;
	bool				IsDefaultModel() const override;
	bool				IsReloadable() const override;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	int					NumJoints() const override;
	const idMD5Joint* 	GetJoints() const override;
	jointHandle_t		GetJointHandle( const char* name ) const override;
	const char* 		GetJointName( jointHandle_t handle ) const override;
	const idJointQuat* 	GetDefaultPose() const override;
	int					NearestJoint( int surfaceNum, int a, int b, int c ) const override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;
	void				ReadFromDemoFile( class idDemoFile* f ) override;
	void				WriteToDemoFile( class idDemoFile* f ) override;
	float				DepthHack() const override;

	bool				ModelHasDrawingSurfaces() const override
	{
		return hasDrawingSurfaces;
	};

	bool				ModelHasInteractingSurfaces() const override
	{
		return hasInteractingSurfaces;
	};

	bool				ModelHasShadowCastingSurfaces() const override
	{
		return hasShadowCastingSurfaces;
	};
	
	void						MakeDefaultModel();
	
	bool						LoadASE( const char* fileName );
	bool						LoadDAE( const char* fileName ); // RB
	bool						LoadLWO( const char* fileName );
	bool						LoadMA( const char* filename );
	
	bool						ConvertDAEToModelSurfaces( const ColladaParser* dae ); // RB
	bool						ConvertASEToModelSurfaces( const struct aseModel_s* ase );
	bool						ConvertLWOToModelSurfaces( const struct st_lwObject* lwo );
	bool						ConvertMAToModelSurfaces( const struct maModel_s* ma );
	
	struct aseModel_s* 			ConvertLWOToASE( const struct st_lwObject* obj, const char* fileName );
	
	bool						DeleteSurfaceWithId( int id );
	void						DeleteSurfacesWithNegativeId();
	bool						FindSurfaceWithId( int id, int& surfaceNum ) const;
	
public:
	idList<modelSurface_t, TAG_MODEL>	surfaces;
	idBounds					bounds;
	int							overlaysAdded;
	
	// when an md5 is instantiated, the inverted joints array is stored to allow GPU skinning
	int							numInvertedJoints;
	idJointMat* 				jointsInverted;
	vertCacheHandle_t			jointsInvertedBuffer;
	
protected:
	int							lastModifiedFrame;
	int							lastArchivedFrame;
	
	idStr						name;
	bool						isStaticWorldModel;
	bool						defaulted;
	bool						purged;					// eventually we will have dynamic reloading
	bool						fastLoad;				// don't generate tangents and shadow data
	bool						reloadable;				// if not, reloadModels won't check timestamp
	bool						levelLoadReferenced;	// for determining if it needs to be freed
	bool						hasDrawingSurfaces;
	bool						hasInteractingSurfaces;
	bool						hasShadowCastingSurfaces;
	ID_TIME_T					timeStamp;
	
	static idCVar				r_mergeModelSurfaces;	// combine model surfaces with the same material
	static idCVar				r_slopVertex;			// merge xyz coordinates this far apart
	static idCVar				r_slopTexCoord;			// merge texture coordinates this far apart
	static idCVar				r_slopNormal;			// merge normals that dot less than this
};

/*
===============================================================================

	MD5 animated model

===============================================================================
*/

class idMD5Mesh
{
	friend class				idRenderModelMD5;
	
public:
	idMD5Mesh();
	~idMD5Mesh();
	
	void						ParseMesh( idLexer& parser, int numJoints, const idJointMat* joints );
	
	int							NumVerts() const
	{
		return numVerts;
	}
	int							NumTris() const
	{
		return numTris;
	}
	
	void						UpdateSurface( const struct renderEntity_s* ent, const idJointMat* joints,
			const idJointMat* entJointsInverted, modelSurface_t* surf );
	void						CalculateBounds( const idJointMat* entJoints, idBounds& bounds ) const;
	int							NearestJoint( int a, int b, int c ) const;
	
private:
	const idMaterial* 			shader;				// material applied to mesh
	int							numVerts;			// number of vertices
	int							numTris;			// number of triangles
	byte* 						meshJoints;			// the joints used by this mesh
	int							numMeshJoints;		// number of mesh joints
	float						maxJointVertDist;	// maximum distance a vertex is separated from a joint
	deformInfo_t* 				deformInfo;			// used to create srfTriangles_t from base frames and new vertexes
	int							surfaceNum;			// number of the static surface created for this mesh
};

class idRenderModelMD5 : public idRenderModelStatic
{
public:
	void				InitFromFile( const char* fileName ) override;
	bool				LoadBinaryModel( idFile* file, const ID_TIME_T sourceTimeStamp ) override;
	void				WriteBinaryModel( idFile* file, ID_TIME_T* _timeStamp = nullptr ) const override;
	dynamicModel_t		IsDynamicModel() const override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;
	void				Print() const override;
	void				List() const override;
	void				TouchData() override;
	void				PurgeModel() override;
	void				LoadModel() override;
	int					Memory() const override;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	int					NumJoints() const override;
	const idMD5Joint* 	GetJoints() const override;
	jointHandle_t		GetJointHandle( const char* name ) const override;
	const char* 		GetJointName( jointHandle_t handle ) const override;
	const idJointQuat* 	GetDefaultPose() const override;
	int					NearestJoint( int surfaceNum, int a, int b, int c ) const override;

	bool				SupportsBinaryModel() override
	{
		return true;
	}
	
private:
	idList<idMD5Joint, TAG_MODEL>	joints;
	idList<idJointQuat, TAG_MODEL>	defaultPose;
	idList<idJointMat, TAG_MODEL>	invertedDefaultPose;
	idList<idMD5Mesh, TAG_MODEL>	meshes;
	
	void						DrawJoints( const renderEntity_t* ent, const viewDef_t* view ) const;
	void						ParseJoint( idLexer& parser, idMD5Joint* joint, idJointQuat* defaultPose );
};

/*
===============================================================================

	MD3 animated model

===============================================================================
*/

struct md3Header_s;
struct md3Surface_s;

class idRenderModelMD3 : public idRenderModelStatic
{
public:
	void				InitFromFile( const char* fileName ) override;

	bool				SupportsBinaryModel() override
	{
		return false;
	}

	dynamicModel_t		IsDynamicModel() const override;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;
	
private:
	int							index;			// model = tr.models[model->index]
	int							dataSize;		// just for listing purposes
	struct md3Header_s* 		md3;			// only if type == MOD_MESH
	int							numLods;
	
	void						LerpMeshVertexes( srfTriangles_t* tri, const struct md3Surface_s* surf, const float backlerp, const int frame, const int oldframe ) const;
};

/*
===============================================================================

	Liquid model

===============================================================================
*/

class idRenderModelLiquid : public idRenderModelStatic
{
public:
	idRenderModelLiquid();

	void				InitFromFile( const char* fileName ) override;

	bool				SupportsBinaryModel() override
	{
		return false;
	}

	dynamicModel_t		IsDynamicModel() const override;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;

	void				Reset() override;
	void						IntersectBounds( const idBounds& bounds, float displacement );
	
private:
	modelSurface_t				GenerateSurface( float lerp );
	void						WaterDrop( int x, int y, float* page );
	void						Update();
	
	int							verts_x;
	int							verts_y;
	float						scale_x;
	float						scale_y;
	int							time;
	int							liquid_type;
	int							update_tics;
	int							seed;
	
	idRandom					random;
	
	const idMaterial* 			shader;
	deformInfo_t* 				deformInfo;		// used to create srfTriangles_t from base frames
	// and new vertexes
	
	float						density;
	float						drop_height;
	int							drop_radius;
	float						drop_delay;
	
	idList<float, TAG_MODEL>	pages;
	float* 						page1;
	float* 						page2;
	
	idList<idDrawVert, TAG_MODEL>	verts;
	
	int							nextDropTime;
	
};

/*
===============================================================================

	PRT model

===============================================================================
*/

class idRenderModelPrt : public idRenderModelStatic
{
public:
	idRenderModelPrt();

	void				InitFromFile( const char* fileName ) override;

	bool				SupportsBinaryModel() override
	{
		return false;
	}

	void				TouchData() override;
	dynamicModel_t		IsDynamicModel() const override;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;
	float				DepthHack() const override;
	int					Memory() const override;
	
	// with the addModels2 arrangement we could have light accepting and
	// shadowing dynamic models, but the original game never did
	bool				ModelHasDrawingSurfaces() const override
	{
		return true;
	};

	bool				ModelHasInteractingSurfaces() const override
	{
		return false;
	};

	bool				ModelHasShadowCastingSurfaces() const override
	{
		return false;
	};
	
private:
	const idDeclParticle* 		particleSystem;
};

/*
===============================================================================

	Beam model

===============================================================================
*/

class idRenderModelBeam : public idRenderModelStatic
{
public:
	dynamicModel_t		IsDynamicModel() const override;

	bool				SupportsBinaryModel() override
	{
		return false;
	}
	virtual bool				IsLoaded() const;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;
	
	// with the addModels2 arrangement we could have light accepting and
	// shadowing dynamic models, but the original game never did
	bool				ModelHasDrawingSurfaces() const override
	{
		return true;
	};

	bool				ModelHasInteractingSurfaces() const override
	{
		return false;
	};

	bool				ModelHasShadowCastingSurfaces() const override
	{
		return false;
	};
};

/*
===============================================================================

	Beam model

===============================================================================
*/
#define MAX_TRAIL_PTS	20

struct Trail_t
{
	int							lastUpdateTime;
	int							duration;
	
	idVec3						pts[MAX_TRAIL_PTS];
	int							numPoints;
};

class idRenderModelTrail : public idRenderModelStatic
{
	idList<Trail_t, TAG_MODEL>	trails;
	int							numActive;
	idBounds					trailBounds;
	
public:
	idRenderModelTrail();

	dynamicModel_t		IsDynamicModel() const override;

	bool				SupportsBinaryModel() override
	{
		return false;
	}
	virtual bool				IsLoaded() const;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;
	
	// with the addModels2 arrangement we could have light accepting and
	// shadowing dynamic models, but the original game never did
	bool				ModelHasDrawingSurfaces() const override
	{
		return true;
	};

	bool				ModelHasInteractingSurfaces() const override
	{
		return false;
	};

	bool				ModelHasShadowCastingSurfaces() const override
	{
		return false;
	};
	
	int							NewTrail( idVec3 pt, int duration );
	void						UpdateTrail( int index, idVec3 pt );
	void						DrawTrail( int index, const struct renderEntity_s* ent, srfTriangles_t* tri, float globalAlpha );
};

/*
===============================================================================

	Lightning model

===============================================================================
*/

class idRenderModelLightning : public idRenderModelStatic
{
public:
	dynamicModel_t		IsDynamicModel() const override;

	bool				SupportsBinaryModel() override
	{
		return false;
	}
	virtual bool				IsLoaded() const;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;
	
	// with the addModels2 arrangement we could have light accepting and
	// shadowing dynamic models, but the original game never did
	bool				ModelHasDrawingSurfaces() const override
	{
		return true;
	};

	bool				ModelHasInteractingSurfaces() const override
	{
		return false;
	};

	bool				ModelHasShadowCastingSurfaces() const override
	{
		return false;
	};
};

/*
================================================================================

	idRenderModelSprite

================================================================================
*/
class idRenderModelSprite : public idRenderModelStatic
{
public:
	dynamicModel_t		IsDynamicModel() const override;

	bool				SupportsBinaryModel() override
	{
		return false;
	}
	virtual	bool				IsLoaded() const;
	idRenderModel* 		InstantiateDynamicModel( const struct renderEntity_s* ent, const viewDef_t* view, idRenderModel* cachedModel ) override;
	idBounds			Bounds( const struct renderEntity_s* ent ) const override;
	
	// with the addModels2 arrangement we could have light accepting and
	// shadowing dynamic models, but the original game never did
	bool				ModelHasDrawingSurfaces() const override
	{
		return true;
	};

	bool				ModelHasInteractingSurfaces() const override
	{
		return false;
	};

	bool				ModelHasShadowCastingSurfaces() const override
	{
		return false;
	};
};

#endif /* !__MODEL_LOCAL_H__ */
