/*
===========================================================================

Doom 3 BFG Edition GPL Source Code
Copyright (C) 1993-2012 id Software LLC, a ZeniMax Media company.
Copyright (C) 2014-2016 Robert Beckebans

This file is part of the Doom 3 BFG Edition GPL Source Code ("Doom 3 BFG Edition Source Code").

Doom 3 BFG Edition Source Code is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Doom 3 BFG Edition Source Code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Doom 3 BFG Edition Source Code.  If not, see <http://www.gnu.org/licenses/>.

In addition, the Doom 3 BFG Edition Source Code is also subject to certain additional terms. You should have received a copy of these additional terms immediately following the terms and conditions of the GNU General Public License which accompanied the Doom 3 BFG Edition Source Code.  If not, please request a copy in writing from id Software at the address below.

If you have questions concerning this license or the applicable additional terms, you may contact in writing id Software LLC, c/o ZeniMax Media Inc., Suite 120, Rockville, Maryland 20850 USA.

===========================================================================
*/
#pragma hdrstop
#include "precompiled.h"
#include "../Game_local.h"

const static int NUM_SYSTEM_OPTIONS_OPTIONS = 8;

/*
========================
idMenuScreen_Shell_ExtraOptions::Initialize
========================
*/
void idMenuScreen_Shell_ExtraOptions::Initialize( idMenuHandler* data )
{
	idMenuScreen::Initialize( data );
	
	if( data  )
	{
		menuGUI = data->GetGUI();
	}
	
	SetSpritePath( "menuSystemOptions" );
	
	options = new( TAG_SWF ) idMenuWidget_DynamicList();
	options->SetNumVisibleOptions( NUM_SYSTEM_OPTIONS_OPTIONS );
	options->SetSpritePath( GetSpritePath(), "info", "options" );
	options->SetWrappingAllowed( true );
	options->SetControlList( true );
	options->Initialize( data );
	
	btnBack = new( TAG_SWF ) idMenuWidget_Button();
	btnBack->Initialize( data );
	btnBack->SetLabel( "#str_swf_settings" );
	btnBack->SetSpritePath( GetSpritePath(), "info", "btnBack" );
	btnBack->AddEventAction( WIDGET_EVENT_PRESS ).Set( WIDGET_ACTION_GO_BACK );
	
	AddChild( options );
	AddChild( btnBack );
	
	idMenuWidget_ControlButton* control;
	control = new( TAG_SWF ) idMenuWidget_ControlButton();
	control->SetOptionType( OPTION_SLIDER_TEXT );
	control->SetLabel( "Soft Shadows" );
	control->SetDescription("Use shadow mapping instead of stencil shadows.");
	control->SetDataSource( &systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_SHADOWMAPPING );
	control->SetupEvents( DEFAULT_REPEAT_TIME, options->GetChildren().Num() );
	control->AddEventAction( WIDGET_EVENT_PRESS ).Set( WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_SHADOWMAPPING );
	options->AddChild( control );

	// AR: buggy
	/*control = new(TAG_SWF) idMenuWidget_ControlButton();
	control->SetOptionType(OPTION_SLIDER_TEXT);
	control->SetLabel("Self Shadows (EXPERMENTAL)");
	control->SetDataSource(&systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_SELFSHADOW);
	control->SetupEvents(DEFAULT_REPEAT_TIME, options->GetChildren().Num());
	control->AddEventAction(WIDGET_EVENT_PRESS).Set(WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_SELFSHADOW);
	options->AddChild(control);*/

	control = new(TAG_SWF) idMenuWidget_ControlButton();
	control->SetOptionType(OPTION_SLIDER_TEXT);
	control->SetLabel("SSAO");
	control->SetDescription("Use Screen-Space Ambient Occlusion");
	control->SetDataSource(&systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_SSAO);
	control->SetupEvents(DEFAULT_REPEAT_TIME, options->GetChildren().Num());
	control->AddEventAction(WIDGET_EVENT_PRESS).Set(WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_SSAO);
	options->AddChild(control);

	// AR: not yet implemented
	/*control = new(TAG_SWF) idMenuWidget_ControlButton();
	control->SetOptionType(OPTION_SLIDER_TEXT);
	control->SetLabel("SSGI");
	control->SetDescription("Use Screen-Space Global Illumination and reflections.");
	control->SetDataSource(&systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_SSGI);
	control->SetupEvents(DEFAULT_REPEAT_TIME, options->GetChildren().Num());
	control->AddEventAction(WIDGET_EVENT_PRESS).Set(WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_SSGI);
	options->AddChild(control);*/

	control = new(TAG_SWF) idMenuWidget_ControlButton();
	control->SetOptionType(OPTION_SLIDER_TEXT);
	control->SetLabel("HDR");
	control->SetDescription("Enable High Dynamic Range rendering.");
	control->SetDataSource(&systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_HDR);
	control->SetupEvents(DEFAULT_REPEAT_TIME, options->GetChildren().Num());
	control->AddEventAction(WIDGET_EVENT_PRESS).Set(WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_HDR);
	options->AddChild(control);

	control = new(TAG_SWF) idMenuWidget_ControlButton();
	control->SetOptionType(OPTION_SLIDER_TEXT);
	//control->SetLabel("HDR's AutoExposure (EXPERIMENTAL)");
	control->SetLabel("HDR's AutoExposure (TEST)");
	control->SetDescription("Use tonemapping with HDR. (Turn this off if you experience graphical glitches");
	control->SetDataSource(&systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_HDR_AUTOEXPOSURE);
	control->SetupEvents(DEFAULT_REPEAT_TIME, options->GetChildren().Num());
	control->AddEventAction(WIDGET_EVENT_PRESS).Set(WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_HDR_AUTOEXPOSURE);
	control->SetDisabled(true);
	control->Update();
	options->AddChild(control);

	control = new(TAG_SWF) idMenuWidget_ControlButton();
	control->SetOptionType(OPTION_SLIDER_TEXT);
	control->SetLabel("Filmic Post-Process");
	control->SetDescription("Apply several post-process effects to mimic a filmic look.");
	control->SetDataSource(&systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_FILMIC_POSTPROCESS);
	control->SetupEvents(DEFAULT_REPEAT_TIME, options->GetChildren().Num());
	control->AddEventAction(WIDGET_EVENT_PRESS).Set(WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_FILMIC_POSTPROCESS);
	control->SetDisabled(true);
	control->Update();
	options->AddChild(control);

	// AR: redundant
	/*control = new(TAG_SWF) idMenuWidget_ControlButton();
	control->SetOptionType(OPTION_SLIDER_TEXT);
	control->SetLabel("Muzzle Flashes");
	control->SetDescription("Use muzzle flashes.");
	control->SetDataSource(&systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_MUZZLE_FLASHES);
	control->SetupEvents(DEFAULT_REPEAT_TIME, options->GetChildren().Num());
	control->AddEventAction(WIDGET_EVENT_PRESS).Set(WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_MUZZLE_FLASHES);
	control->SetDisabled(true);
	control->Update();
	options->AddChild(control);	*/
	// AR end

	control = new(TAG_SWF) idMenuWidget_ControlButton();
	control->SetOptionType(OPTION_SLIDER_TEXT);
	control->SetLabel("Allow Console");
	control->SetDescription("Allow usage of console.");
	control->SetDataSource(&systemData, idMenuDataSource_ExtraSettings::EXTRA_FIELD_ALLOW_CONSOLE);
	control->SetupEvents(DEFAULT_REPEAT_TIME, options->GetChildren().Num());
	control->AddEventAction(WIDGET_EVENT_PRESS).Set(WIDGET_ACTION_COMMAND, idMenuDataSource_ExtraSettings::EXTRA_FIELD_ALLOW_CONSOLE);
	control->SetDisabled(true);
	control->Update();
	options->AddChild(control);
	
	options->AddEventAction( WIDGET_EVENT_SCROLL_DOWN ).Set( new( TAG_SWF ) idWidgetActionHandler( options, WIDGET_ACTION_EVENT_SCROLL_DOWN_START_REPEATER, WIDGET_EVENT_SCROLL_DOWN ) );
	options->AddEventAction( WIDGET_EVENT_SCROLL_UP ).Set( new( TAG_SWF ) idWidgetActionHandler( options, WIDGET_ACTION_EVENT_SCROLL_UP_START_REPEATER, WIDGET_EVENT_SCROLL_UP ) );
	options->AddEventAction( WIDGET_EVENT_SCROLL_DOWN_RELEASE ).Set( new( TAG_SWF ) idWidgetActionHandler( options, WIDGET_ACTION_EVENT_STOP_REPEATER, WIDGET_EVENT_SCROLL_DOWN_RELEASE ) );
	options->AddEventAction( WIDGET_EVENT_SCROLL_UP_RELEASE ).Set( new( TAG_SWF ) idWidgetActionHandler( options, WIDGET_ACTION_EVENT_STOP_REPEATER, WIDGET_EVENT_SCROLL_UP_RELEASE ) );
	options->AddEventAction( WIDGET_EVENT_SCROLL_DOWN_LSTICK ).Set( new( TAG_SWF ) idWidgetActionHandler( options, WIDGET_ACTION_EVENT_SCROLL_DOWN_START_REPEATER, WIDGET_EVENT_SCROLL_DOWN_LSTICK ) );
	options->AddEventAction( WIDGET_EVENT_SCROLL_UP_LSTICK ).Set( new( TAG_SWF ) idWidgetActionHandler( options, WIDGET_ACTION_EVENT_SCROLL_UP_START_REPEATER, WIDGET_EVENT_SCROLL_UP_LSTICK ) );
	options->AddEventAction( WIDGET_EVENT_SCROLL_DOWN_LSTICK_RELEASE ).Set( new( TAG_SWF ) idWidgetActionHandler( options, WIDGET_ACTION_EVENT_STOP_REPEATER, WIDGET_EVENT_SCROLL_DOWN_LSTICK_RELEASE ) );
	options->AddEventAction( WIDGET_EVENT_SCROLL_UP_LSTICK_RELEASE ).Set( new( TAG_SWF ) idWidgetActionHandler( options, WIDGET_ACTION_EVENT_STOP_REPEATER, WIDGET_EVENT_SCROLL_UP_LSTICK_RELEASE ) );
}

/*
========================
idMenuScreen_Shell_ExtraOptions::Update
========================
*/
void idMenuScreen_Shell_ExtraOptions::Update()
{

	if( menuData  )
	{
		idMenuWidget_CommandBar* cmdBar = menuData->GetCmdBar();
		if( cmdBar  )
		{
			cmdBar->ClearAllButtons();
			idMenuWidget_CommandBar::buttonInfo_t* buttonInfo;
			buttonInfo = cmdBar->GetButton( idMenuWidget_CommandBar::BUTTON_JOY2 );
			if( menuData->GetPlatform() != 2 )
			{
				buttonInfo->label = "#str_00395";
			}
			buttonInfo->action.Set( WIDGET_ACTION_GO_BACK );
			
			buttonInfo = cmdBar->GetButton( idMenuWidget_CommandBar::BUTTON_JOY1 );
			buttonInfo->action.Set( WIDGET_ACTION_PRESS_FOCUSED );
		}
	}
	
	idSWFScriptObject& root = GetSWFObject()->GetRootObject();
	if( BindSprite( root ) )
	{
		idSWFTextInstance* heading = GetSprite()->GetScriptObject()->GetNestedText( "info", "txtHeading" );
		if( heading  )
		{
			heading->SetText( "Extra" );	// FULLSCREEN
			heading->SetStrokeInfo( true, 0.75f, 1.75f );
		}
		
		idSWFSpriteInstance* gradient = GetSprite()->GetScriptObject()->GetNestedSprite( "info", "gradient" );
		if( gradient  && heading  )
		{
			gradient->SetXPos( heading->GetTextLength() );
		}
	}
	
	if( btnBack  )
	{
		btnBack->BindSprite( root );
	}
	
	idMenuScreen::Update();
}

/*
========================
idMenuScreen_Shell_ExtraOptions::ShowScreen
========================
*/
void idMenuScreen_Shell_ExtraOptions::ShowScreen( const mainMenuTransition_t transitionType )
{

	systemData.LoadData();
	
	idMenuScreen::ShowScreen( transitionType );
}

/*
========================
idMenuScreen_Shell_ExtraOptions::HideScreen
========================
*/
void idMenuScreen_Shell_ExtraOptions::HideScreen( const mainMenuTransition_t transitionType )
{

	if( systemData.IsRestartRequired() )
	{
		class idSWFScriptFunction_Restart : public idSWFScriptFunction_RefCounted
		{
		public:
			idSWFScriptFunction_Restart( gameDialogMessages_t _msg, bool _restart )
			{
				msg = _msg;
				restart = _restart;
			}
			idSWFScriptVar Call( idSWFScriptObject* thisObject, const idSWFParmList& parms ) override
			{
				common->Dialog().ClearDialog( msg );
				if( restart )
				{
					// DG: Sys_ReLaunch() doesn't need any options anymore
					//     (the old way would have been unnecessarily painful on POSIX systems)
					Sys_ReLaunch();
					// DG end
				}
				return idSWFScriptVar();
			}
		private:
			gameDialogMessages_t msg;
			bool restart;
		};
		idStaticList<idSWFScriptFunction*, 4> callbacks;
		idStaticList<idStrId, 4> optionText;
		callbacks.Append( new idSWFScriptFunction_Restart( GDM_GAME_RESTART_REQUIRED, false ) );
		callbacks.Append( new idSWFScriptFunction_Restart( GDM_GAME_RESTART_REQUIRED, true ) );
		optionText.Append( idStrId( "#str_00100113" ) ); // Continue
		optionText.Append( idStrId( "#str_02487" ) ); // Restart Now
		common->Dialog().AddDynamicDialog( GDM_GAME_RESTART_REQUIRED, callbacks, optionText, true, idStr() );
	}
	
	if( systemData.IsDataChanged() )
	{
		systemData.CommitData();
	}
	
	idMenuScreen::HideScreen( transitionType );
}

/*
========================
idMenuScreen_Shell_ExtraOptions::HandleAction h
========================
*/
bool idMenuScreen_Shell_ExtraOptions::HandleAction( idWidgetAction& action, const idWidgetEvent& event, idMenuWidget* widget, bool forceHandled )
{

	if( menuData == nullptr )
	{
		return true;
	}
	
	if( menuData->ActiveScreen() != SHELL_AREA_EXTRA_OPTIONS )
	{
		return false;
	}
	
	widgetAction_t actionType = action.GetType();
	const idSWFParmList& parms = action.GetParms();
	
	switch( actionType )
	{
		case WIDGET_ACTION_GO_BACK:
		{
			if( menuData  )
			{
				menuData->SetNextScreen( SHELL_AREA_SETTINGS, MENU_TRANSITION_SIMPLE );
			}
			return true;
		}
		case WIDGET_ACTION_ADJUST_FIELD:
			// AR: preparation for new menu 
			/*if( widget->GetDataSourceFieldIndex() == idMenuDataSource_ExtraSettings::EXTRA_FIELD_HDROPTIONS )
			{
				menuData->SetNextScreen( SHELL_AREA_HDROPTIONS, MENU_TRANSITION_SIMPLE );
				return true;
			}*/
			break;
		case WIDGET_ACTION_COMMAND:
		{
		
			if( options == nullptr )
			{
				return true;
			}
			
			int selectionIndex = options->GetFocusIndex();
			if( parms.Num() > 0 )
			{
				selectionIndex = parms[0].ToInteger();
			}
			
			if( options && selectionIndex != options->GetFocusIndex() )
			{
				options->SetViewIndex( options->GetViewOffset() + selectionIndex );
				options->SetFocusIndex( selectionIndex );
			}
			
			// AR: preparation for new sub menu
			/*switch( parms[0].ToInteger() )
			{
				case idMenuDataSource_ExtraSettings::EXTRA_FIELD_HDROPTIONS:
				{
					menuData->SetNextScreen( SHELL_AREA_HDROPTIONS, MENU_TRANSITION_SIMPLE );
					return true;
				}
				default:
				{
					systemData.AdjustField( parms[0].ToInteger(), 1 );
					options->Update();
				}
			}*/
			// AR: temporary placeholder
			systemData.AdjustField( parms[0].ToInteger(), 1 );
			options->Update();
			// AR end

			return true;
		}
		case WIDGET_ACTION_START_REPEATER:
		{
		
			if( !options )
				return true;
			
			if( parms.Num() == 4 )
			{
				int selectionIndex = parms[3].ToInteger();
				if( selectionIndex != options->GetFocusIndex() )
				{
					options->SetViewIndex( options->GetViewOffset() + selectionIndex );
					options->SetFocusIndex( selectionIndex );
				}
			}
			break;
		}
	}
	
	return idMenuWidget::HandleAction( action, event, widget, forceHandled );
}

/////////////////////////////////
// SCREEN SETTINGS
/////////////////////////////////

/*
========================
idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::idMenuDataSource_ExtraSettings
========================
*/
idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::idMenuDataSource_ExtraSettings()
{
}

/*
========================
idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::LoadData
========================
*/
void idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::LoadData()
{
	originalShadowMapping = r_useShadowMapping.GetBool();
	//originalSelfShadow = r_selfShadow.GetBool();
	originalSSAO = r_useSSAO.GetBool();
	//originalSSGI = r_useSSGI.GetBool();
	originalHDR = r_useHDR.GetBool();
	originalHDRAutoExposure = r_hdrAutoExposure.GetBool();
	originalFilmicPostProcess = r_useFilmicPostProcessEffects.GetBool();
	//originalMuzzleFlashes = g_muzzleFlash.GetBool();
	originalAllowConsole = com_allowConsole.GetBool();
}

/*
========================
idMenuScreen_Shell_SystemOptions::idMenuDataSource_SystemSettings::IsRestartRequired
========================
*/
bool idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::IsRestartRequired() const
{
	if (originalShadowMapping != r_useShadowMapping.GetBool())
		return true;
	
	// AR: buggy
	//if (originalSelfShadow != r_selfShadow.GetBool())
	//	return true;
	
	// AR: redundant
	//if (originalSSAO != r_useSSAO.GetBool())
	//	return true;

	// AR: not yet implemented
	//if (originalSSGI != r_useSSGI.GetBool())
	//	return true;

	if (originalHDR != r_useHDR.GetBool())
		return true;

	if (originalHDRAutoExposure != r_hdrAutoExposure.GetBool())
		return true;

	if (originalFilmicPostProcess != r_useFilmicPostProcessEffects.GetBool())
		return true;

	/*if (originalMuzzleFlashes != g_muzzleFlash.GetBool())
		return true;*/

	if (originalAllowConsole != com_allowConsole.GetBool())
		return true;

	return false;
}

/*
========================
idMenuScreen_Shell_SystemOptions::idMenuDataSource_SystemSettings::CommitData
========================
*/
void idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::CommitData()
{
	cvarSystem->SetModifiedFlags( CVAR_ARCHIVE );
}

/*
========================
idMenuScreen_Shell_SystemOptions::idMenuDataSource_SystemSettings::AdjustField
========================
*/
void idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::AdjustField( const int fieldIndex, const int adjustAmount )
{
	static const int numValues = 2;
	static const int values[numValues] = { 0, 1 };

	switch( fieldIndex )
	{
		case EXTRA_FIELD_SHADOWMAPPING:
		{
			r_useShadowMapping.SetBool( AdjustOption( r_useShadowMapping.GetBool(), values, numValues, adjustAmount ) );
			break;
		}
		// AR: buggy
		/*case EXTRA_FIELD_SELFSHADOW:
		{
			r_selfShadow.SetBool(AdjustOption(r_selfShadow.GetBool(), values, numValues, adjustAmount));
			break;
		}*/
		case EXTRA_FIELD_SSAO:
		{
			r_useSSAO.SetBool(AdjustOption(r_useSSAO.GetBool(), values, numValues, adjustAmount));
			break;
		}
		// AR: not yet implemented
		/*case EXTRA_FIELD_SSGI:
		{
			r_useSSGI.SetBool(AdjustOption(r_useSSGI.GetBool(), values, numValues, adjustAmount));
			break;
		}*/
		case EXTRA_FIELD_HDR:
		{
			r_useHDR.SetBool(AdjustOption(r_useHDR.GetBool(), values, numValues, adjustAmount));
			break;
		}
		case EXTRA_FIELD_HDR_AUTOEXPOSURE:
		{
			r_hdrAutoExposure.SetBool(AdjustOption(r_hdrAutoExposure.GetBool(), values, numValues, adjustAmount));
			break;
		}
		case EXTRA_FIELD_FILMIC_POSTPROCESS:
		{
			r_useFilmicPostProcessEffects.SetBool(AdjustOption(r_useFilmicPostProcessEffects.GetBool(), values, numValues, adjustAmount));
			break;
		}
		/*case EXTRA_FIELD_MUZZLE_FLASHES:
		{
			g_muzzleFlash.SetBool(AdjustOption(g_muzzleFlash.GetBool(), values, numValues, adjustAmount));
			break;
		}*/
		case EXTRA_FIELD_ALLOW_CONSOLE:
		{
			com_allowConsole.SetBool(AdjustOption(com_allowConsole.GetBool(), values, numValues, adjustAmount));
			break;
		}
	}
	cvarSystem->ClearModifiedFlags( CVAR_ARCHIVE );
}

/*
========================
idMenuScreen_Shell_SystemOptions::idMenuDataSource_SystemSettings::GetField
========================
*/
idSWFScriptVar idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::GetField( const int fieldIndex ) const
{
	switch( fieldIndex )
	{
		case EXTRA_FIELD_SHADOWMAPPING:
			if( r_useShadowMapping.GetBool() )
				return "#str_swf_enabled";
			return "#str_swf_disabled";

		// AR: buggy
		/*case EXTRA_FIELD_SELFSHADOW:
			if (r_selfShadow.GetBool())
				return "#str_swf_enabled";
			return "#str_swf_disabled";*/

		case EXTRA_FIELD_SSAO:
			if (r_useSSAO.GetBool())
				return "#str_swf_enabled";
			return "#str_swf_disabled";

		// AR: not yet implemented
		/*case EXTRA_FIELD_SSGI:
			if (r_useSSGI.GetBool())
				return "#str_swf_enabled";
			return "#str_swf_disabled";*/

		case EXTRA_FIELD_HDR:
			if (r_useHDR.GetBool())
				return "#str_swf_enabled";
			return "#str_swf_disabled";

		case EXTRA_FIELD_HDR_AUTOEXPOSURE:
			if (r_hdrAutoExposure.GetBool())
				return "#str_swf_enabled";
			return "#str_swf_disabled";

		case EXTRA_FIELD_FILMIC_POSTPROCESS:
			if (r_useFilmicPostProcessEffects.GetBool())
				return "#str_swf_enabled";
			return "#str_swf_disabled";

		/*case EXTRA_FIELD_MUZZLE_FLASHES:
			if (g_muzzleFlash.GetBool())
				return "#str_swf_enabled";
			return "#str_swf_disabled";*/

		case EXTRA_FIELD_ALLOW_CONSOLE:
			if (com_allowConsole.GetBool())
				return "#str_swf_enabled";
			return "#str_swf_disabled";
	}
	return false;
}

/*
========================
idMenuScreen_Shell_SystemOptions::idMenuDataSource_SystemSettings::IsDataChanged
========================
*/
bool idMenuScreen_Shell_ExtraOptions::idMenuDataSource_ExtraSettings::IsDataChanged() const
{
	if( originalShadowMapping != r_useShadowMapping.GetBool() )
		return true;

	// AR: buggy
	/*if (originalSelfShadow != r_selfShadow.GetBool())
		return true;*/

	if (originalSSAO != r_useSSAO.GetBool())
		return true;

	// AR: not yet implemented
	//if (originalSSGI != r_useSSGI.GetBool())
	//	return true;

	if (originalHDR != r_useHDR.GetBool())
		return true;

	if (originalHDRAutoExposure != r_hdrAutoExposure.GetBool())
		return true;

	if (originalFilmicPostProcess != r_useFilmicPostProcessEffects.GetBool())
		return true;

	/*if (originalMuzzleFlashes != g_muzzleFlash.GetBool())
		return true;*/

	if (originalAllowConsole != com_allowConsole.GetBool())
		return true;

	return false;
}
